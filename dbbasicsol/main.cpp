#include <assert.h>
#include <iostream>
#include <sstream>

#include "SFML/Graphics.hpp"
#include "Utils.h"
#include "..\..\sqlite\sqlite3.h"

using namespace sf;
using namespace std;


static int callback(void *NotUsed, int argc, char **argv, char **azColName) {
	stringstream ss;
	int i;
	for (i = 0; i < argc; i++) {
		ss << azColName[i] << " = " << (argv[i] ? argv[i] : "NULL") << "\n";
	}
	DebugPrint(ss.str().c_str(), "");
	return 0;
}

int gNumRecords = 0;
static int callbackRecords(void *NotUsed, int argc, char **argv, char **azColName) {
	gNumRecords++;
	return 0;
}

/*
Run queries with less fuss, but all it does
is print out the results to the debug window
*/
void RunQuery(const string& sql, sqlite3& db)
{
	DebugPrint("\nRun SQL=> ", sql);
	char *zErrMsg = 0;
	int rc = sqlite3_exec(&db, sql.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK) {
		DebugPrint("SQL error: ", zErrMsg);
		sqlite3_free(zErrMsg);
	}
	else {
		DebugPrint("Finished OK");
	}
}

/*
Run some queries that should fail due to foreign key constraints
Delete things in the correct order to keep foreign keys valid
*/
void TestIntegrity(sqlite3& db)
{
	char *zErrMsg = 0;

	//recover the records
	const string sql5 = "SELECT * from EMPLOYEES";
	int rc = sqlite3_exec(&db, sql5.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK) {
		DebugPrint("SQL error: ", zErrMsg);
		sqlite3_free(zErrMsg);
	}
	else {
		DebugPrint("SQL select complete OK", "");
	}

	//delete the QA department - not allowed!
	const string sql11 = "DELETE FROM DEPARTMENTS WHERE NAME='QA_test'";
	rc = sqlite3_exec(&db, sql11.c_str(), nullptr, 0, &zErrMsg);
	if (rc != SQLITE_OK) {
		DebugPrint("SQL error: ", zErrMsg);
		sqlite3_free(zErrMsg);
	}
	else {
		DebugPrint("Department QA_test deletede OK, which is not correct!");
	}


	//change Allen's salary
	const string sql6 = "UPDATE EMPLOYEES SET SALARY = 10000 WHERE NAME = 'Allen'";
	rc = sqlite3_exec(&db, sql6.c_str(), nullptr, 0, &zErrMsg);
	if (rc != SQLITE_OK) {
		DebugPrint("SQL error: ", zErrMsg);
		sqlite3_free(zErrMsg);
	}
	else {
		DebugPrint("Data updated OK", "");
	}



	//terminate Allen
	const string sql7 = "DELETE FROM EMPLOYEES WHERE NAME = 'Allen'";
	rc = sqlite3_exec(&db, sql7.c_str(), nullptr, 0, &zErrMsg);
	if (rc != SQLITE_OK) {
		DebugPrint("SQL error: ", zErrMsg);
		sqlite3_free(zErrMsg);
	}
	else if (sqlite3_changes(&db) > 0) {
		DebugPrint("Allen deleted OK", "");
	}
	else {
		DebugPrint("Nothing to delete");
	}

	//delete the QA department now nobody works there
	rc = sqlite3_exec(&db, sql11.c_str(), nullptr, 0, &zErrMsg);
	if (rc != SQLITE_OK) {
		DebugPrint("SQL error: ", zErrMsg);
		sqlite3_free(zErrMsg);
	}
	else {
		DebugPrint("Department QA_test deletede OK, which is right as Allen was fired!");
	}
}

/*
* Some examples of different SQL queries from simple to complex
* Look in the output window
*/
int main()
{
	// Create the main window
	RenderWindow window(VideoMode(1200, 800), "database");
	sf::Font font;
	if (!font.loadFromFile("data/fonts/comic.ttf"))
		assert(false);

	DebugPrint("Database starter app");

	//create or open the database
	sqlite3 *db;
	int rc = sqlite3_open("data/test.db", &db);
	if (rc) {
		DebugPrint( "Can't open database: ", sqlite3_errmsg(db));
		assert(false);
	}

	char *zErrMsg = 0;	
	rc = sqlite3_exec(db, "PRAGMA foreign_keys = ON;", nullptr, 0, &zErrMsg);
	if (rc != SQLITE_OK) {
		DebugPrint("SQL error: ", zErrMsg);
		sqlite3_free(zErrMsg);
	}

	gNumRecords = 0;
	const string sql3 = "SELECT name FROM sqlite_master WHERE type = 'table' AND name = 'EMPLOYEES' OR name = 'DEPARTMENTS'";
	rc = sqlite3_exec(db, sql3.c_str(), callbackRecords, 0, &zErrMsg);
	if (rc != SQLITE_OK) {
		DebugPrint("SQL error: ", zErrMsg);
		sqlite3_free(zErrMsg);
	}
	else if (gNumRecords != 2) {
		const string sql7 = "CREATE TABLE DEPARTMENTS(" \
			"ID INTEGER PRIMARY KEY NOT NULL," \
			"NAME			TEXT	NOT NULL," \
			"DESCRIPTION	CHAR(100)," \
			"ROOM_NUMBER	INTEGER	NOT NULL);";
		rc = sqlite3_exec(db, sql7.c_str(), nullptr, 0, &zErrMsg);
		if (rc != SQLITE_OK) {
			DebugPrint("SQL error: ", zErrMsg);
			sqlite3_free(zErrMsg);
		}
		else {
			DebugPrint("Department table created successfully");
		}

		/* Create a table SQL statement */
		const string sql = "CREATE TABLE EMPLOYEES("  \
			"ID INTEGER PRIMARY KEY     NOT NULL," \
			"NAME           TEXT    NOT NULL," \
			"AGE            INTEGER     NOT NULL," \
			"ADDRESS        CHAR(50)," \
			"SALARY         REAL," \
			"DEPARTMENT		INTEGER		NOT NULL," \
			"FOREIGN KEY(DEPARTMENT) REFERENCES DEPARTMENTS(ID));";
		rc = sqlite3_exec(db, sql.c_str(), nullptr, 0, &zErrMsg);
		if (rc != SQLITE_OK) {
			DebugPrint("SQL error: ", zErrMsg);
			sqlite3_free(zErrMsg);
		}
		else {
			DebugPrint("Employee table created successfully");
		}
	}

	//department records
	gNumRecords = 0;
	const string sql8 = "SELECT ID FROM DEPARTMENTS";
	rc = sqlite3_exec(db, sql8.c_str(), callbackRecords, 0, &zErrMsg);
	if (rc != SQLITE_OK) {
		DebugPrint("SQL error: ", zErrMsg);
		sqlite3_free(zErrMsg);
	}
	else if (gNumRecords == 0) {
		//insert some records
		const string sql9 = "INSERT INTO DEPARTMENTS (NAME,DESCRIPTION,ROOM_NUMBER) "  \
			"VALUES ('DEV_OPS', 'Game development', 10 ); " \
			"INSERT INTO DEPARTMENTS (NAME,DESCRIPTION,ROOM_NUMBER)" \
			"VALUES ('QA_test', 'Testing and quality control', 11 );";
		rc = sqlite3_exec(db, sql9.c_str(), nullptr, 0, &zErrMsg);
		if (rc != SQLITE_OK) {
			DebugPrint("SQL error: ", zErrMsg);
			sqlite3_free(zErrMsg);
		}
		else {
			DebugPrint("Data inserted successfully into department table");
		}
	}

	//employee records
	gNumRecords = 0;
	const string sql4 = "SELECT ID FROM EMPLOYEES";
	rc = sqlite3_exec(db, sql4.c_str(), callbackRecords, 0, &zErrMsg);
	if (rc != SQLITE_OK) {
		DebugPrint("SQL error: ", zErrMsg);
		sqlite3_free(zErrMsg);
	}
	else if (gNumRecords == 0) {
		//try to insert a bad record
		const string sql10 = "INSERT INTO EMPLOYEES (ID,NAME,AGE,ADDRESS,SALARY, DEPARTMENT) "  \
			"VALUES (1, 'Paul', 32, 'California', 20000.00, 100 ); ";
		rc = sqlite3_exec(db, sql10.c_str(), nullptr, 0, &zErrMsg);
		if (rc != SQLITE_OK) {
			DebugPrint("SQL error: ", zErrMsg);
			sqlite3_free(zErrMsg);
		}
		else {
			DebugPrint("Data insert successfully, which is wrong!");
		}

		//insert some records
		const string sql2 = "INSERT INTO EMPLOYEES (ID,NAME,AGE,ADDRESS,SALARY, DEPARTMENT) "  \
			"VALUES (1, 'Paul', 32, 'California', 20000.00, 1 ); " \
			"INSERT INTO EMPLOYEES (ID,NAME,AGE,ADDRESS,SALARY, DEPARTMENT) "  \
			"VALUES (2, 'Allen', 25, 'Texas', 15000.00, 2 ); "     \
			"INSERT INTO EMPLOYEES (ID,NAME,AGE,ADDRESS,SALARY, DEPARTMENT)" \
			"VALUES (3, 'Teddy', 23, 'Norway', 20000.00, 1 );" \
			"INSERT INTO EMPLOYEES (ID,NAME,AGE,ADDRESS,SALARY, DEPARTMENT)" \
			"VALUES (4, 'Mark', 25, 'Rich-Mond ', 65000.00, 1 );";
		rc = sqlite3_exec(db, sql2.c_str(), nullptr, 0, &zErrMsg);
		if (rc != SQLITE_OK) {
			DebugPrint("SQL error: ", zErrMsg);
			sqlite3_free(zErrMsg);
		}
		else {
			DebugPrint("Data inserted successfully into employee table");
		}
	}

	//TestIntegrity(*db);

	RunQuery("SELECT * FROM EMPLOYEES", *db);
	RunQuery("SELECT * FROM DEPARTMENTS", *db);
	RunQuery("SELECT EMPLOYEES.NAME FROM EMPLOYEES " \
		"INNER JOIN DEPARTMENTS " \
		"ON EMPLOYEES.DEPARTMENT = DEPARTMENTS.ID "\
		"WHERE DEPARTMENTS.NAME = 'QA_test'" \
		"ORDER BY EMPLOYEES.NAME DESC;", *db);

	RunQuery("SELECT EMPLOYEES.NAME FROM EMPLOYEES " \
		"INNER JOIN DEPARTMENTS " \
		"ON EMPLOYEES.DEPARTMENT = DEPARTMENTS.ID "\
		"WHERE DEPARTMENTS.NAME = 'DEV_OPS'" \
		"ORDER BY EMPLOYEES.NAME DESC;", *db);

	RunQuery("SELECT DEPARTMENTS.NAME, "\
		"( SELECT COUNT(*) FROM EMPLOYEES WHERE DEPARTMENT = DEPARTMENTS.ID ) AS Num_Employees "\
		" FROM DEPARTMENTS" \
		" ORDER BY DEPARTMENTS.NAME DESC", *db);

	RunQuery("SELECT DEPARTMENTS.NAME, "\
		"'�'||( SELECT SUM(EMPLOYEES.SALARY) FROM EMPLOYEES WHERE DEPARTMENT = DEPARTMENTS.ID ) AS Staff_Cost "\
		" FROM DEPARTMENTS" \
		" ORDER BY DEPARTMENTS.NAME DESC", *db);

	RunQuery("SELECT DEPARTMENTS.NAME, e.NAME, e.SALARY " \
			"FROM EMPLOYEES e " \
			"INNER JOIN (SELECT MAX(EMPLOYEES.SALARY) ms, EMPLOYEES.DEPARTMENT " \
						"FROM EMPLOYEES GROUP BY DEPARTMENT) m " \
			"ON e.DEPARTMENT = m.DEPARTMENT AND e.SALARY = m.ms " \
			"INNER JOIN DEPARTMENTS "\
			"ON e.DEPARTMENT = DEPARTMENTS.ID", *db);

	//we got a bit carried away above, it works, but this is faster (possibly)
	RunQuery("SELECT DEPARTMENTS.NAME, EMPLOYEES.NAME, MAX(EMPLOYEES.SALARY) ms " \
		"FROM EMPLOYEES "\
		"INNER JOIN DEPARTMENTS "\
		"ON EMPLOYEES.DEPARTMENT = DEPARTMENTS.ID "\
		"GROUP BY DEPARTMENT", *db);


	sqlite3_close(db);


	Clock clock;
	// Start the game loop 
	while (window.isOpen())
	{
		bool fire = false;
		// Process events
		Event event;
		while (window.pollEvent(event))
		{
			// Close window: exit
			if (event.type == Event::Closed) 
				window.close();
		} 

		// Clear screen
		window.clear();

		float elapsed = clock.getElapsedTime().asSeconds();
		clock.restart();

		sf::Text txt("Database starter app", font, 30);
		window.draw(txt);


		// Update the window
		window.display();
	}

	return EXIT_SUCCESS;
}
